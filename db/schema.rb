# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180410115629) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "adresses", force: :cascade do |t|
    t.string "label"
    t.string "short_name"
    t.string "full_name"
    t.boolean "removed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bank_cards", force: :cascade do |t|
    t.string "label"
    t.string "short_name"
    t.string "full_name"
    t.integer "card_number"
    t.boolean "removed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "carts", force: :cascade do |t|
    t.string "label"
    t.string "short_name"
    t.string "full_name"
    t.boolean "removed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cities", force: :cascade do |t|
    t.string "label"
    t.string "short_name"
    t.string "full_name"
    t.boolean "removed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade do |t|
    t.string "label"
    t.string "short_name"
    t.string "full_name"
    t.string "text"
    t.boolean "removed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "countries", force: :cascade do |t|
    t.string "label"
    t.string "full_name"
    t.string "short_name"
    t.boolean "removed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "homes", force: :cascade do |t|
    t.string "label"
    t.string "short_name"
    t.string "full_name"
    t.string "home_number"
    t.boolean "removed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "image_groups", force: :cascade do |t|
    t.string "label"
    t.string "short_name"
    t.string "full_name"
    t.string "description"
    t.boolean "removed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "images", force: :cascade do |t|
    t.string "label"
    t.string "short_name"
    t.string "full_name"
    t.string "description"
    t.string "url"
    t.boolean "removed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "languages", force: :cascade do |t|
    t.string "label"
    t.string "full_name"
    t.string "short_name"
    t.boolean "removed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "operating_systems", force: :cascade do |t|
    t.string "label"
    t.string "full_name"
    t.string "short_name"
    t.boolean "removed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade do |t|
    t.string "label"
    t.string "short_name"
    t.string "full_name"
    t.integer "full_price"
    t.integer "price_with_discount"
    t.integer "total_price"
    t.boolean "issued", default: true
    t.boolean "paid_for", default: false
    t.boolean "canceled", default: false
    t.boolean "removed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "prices", force: :cascade do |t|
    t.string "label"
    t.string "short_name"
    t.string "full_name"
    t.integer "value"
    t.integer "share_of_developer"
    t.integer "share_of_publisher"
    t.boolean "discount_is_possible", default: false
    t.boolean "removed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_tags", force: :cascade do |t|
    t.string "label"
    t.string "short_name"
    t.string "full_name"
    t.string "description"
    t.string "value"
    t.boolean "removed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "products", force: :cascade do |t|
    t.string "label"
    t.string "full_name"
    t.string "short_name"
    t.boolean "removed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "raitings", force: :cascade do |t|
    t.string "label"
    t.string "short_name"
    t.string "full_name"
    t.boolean "alcohol_reference"
    t.boolean "animated_blood"
    t.boolean "blood"
    t.boolean "blood_and_gore"
    t.boolean "cartoon_violence"
    t.boolean "comic_mischief"
    t.boolean "crude_humor"
    t.boolean "grug_reference"
    t.boolean "edutainment"
    t.boolean "fantasy_violence"
    t.boolean "informational"
    t.boolean "intense_violence"
    t.boolean "cyrics"
    t.boolean "mature_humor"
    t.boolean "mild_lyrics"
    t.boolean "mild_language"
    t.boolean "mild_suggestive_themes"
    t.boolean "mild_violence"
    t.boolean "nudity"
    t.boolean "partial_nudity"
    t.boolean "real_gambling"
    t.boolean "sexual_themes"
    t.boolean "simulated_gambling"
    t.boolean "some_adult_assistance_may_be_need"
    t.boolean "strong_language"
    t.boolean "strong_lyrics"
    t.boolean "strong_sexual_content"
    t.boolean "suggestive_themes"
    t.boolean "tobacco_reference"
    t.boolean "use_of_drugs"
    t.boolean "use_of_tobacoo"
    t.boolean "violence"
    t.boolean "removed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "residential_areas", force: :cascade do |t|
    t.string "label"
    t.string "short_name"
    t.string "full_name"
    t.integer "area_number"
    t.boolean "removed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "special_tags", force: :cascade do |t|
    t.string "label"
    t.string "short_name"
    t.string "full_name"
    t.string "description"
    t.string "value"
    t.boolean "removed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "streets", force: :cascade do |t|
    t.string "label"
    t.string "short_name"
    t.string "full_name"
    t.integer "street_number"
    t.boolean "removed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "system_requirements", force: :cascade do |t|
    t.string "processor"
    t.integer "memory"
    t.string "graphics"
    t.integer "hard_drive"
    t.boolean "mouse"
    t.boolean "keyboard"
    t.boolean "removed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_profiles", force: :cascade do |t|
    t.string "first_name"
    t.string "patronymic"
    t.string "last_name"
    t.boolean "is_male"
    t.boolean "is_female"
    t.integer "age"
    t.string "status"
    t.string "about"
    t.boolean "removed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.bigint "phone"
    t.string "email"
    t.string "password"
    t.boolean "removed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email", "username", "phone"], name: "index_users_on_email_and_username_and_phone", unique: true
  end

  create_table "value_types", force: :cascade do |t|
    t.string "label"
    t.string "short_name"
    t.string "full_name"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end

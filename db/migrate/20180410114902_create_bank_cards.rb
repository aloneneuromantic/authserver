class CreateBankCards < ActiveRecord::Migration[5.1]
  def change
    create_table :bank_cards do |t|
      # General table columns
      t.string :label
      t.string :short_name
      t.string :full_name
      t.integer :card_number
      # Special table columns
      t.boolean :removed
      t.timestamps
    end
  end
end

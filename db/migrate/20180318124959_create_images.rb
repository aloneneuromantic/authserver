class CreateImages < ActiveRecord::Migration[5.1]
  def change
    create_table :images do |t|
      # General table columns
      t.string :label
      t.string :short_name
      t.string :full_name
      t.string :description
      t.string :url
      # Special table columns
      t.boolean :removed
      t.timestamps
    end
  end
end

class CreateResidentialAreas < ActiveRecord::Migration[5.1]
  def change
    create_table :residential_areas do |t|
      # General table columns
      t.string :label
      t.string :short_name
      t.string :full_name
      t.integer :area_number
      # Special table columns
      t.boolean :removed
      t.timestamps
    end
  end
end

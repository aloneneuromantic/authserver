class CreateUserProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :user_profiles do |t|
      # General table columns
      t.string :first_name
      t.string :patronymic
      t.string :last_name
      t.boolean :is_male
      t.boolean :is_female
      t.integer :age
      t.string :status
      t.string :about
      # Special table columns
      t.boolean :removed
      t.timestamps
    end
  end
end

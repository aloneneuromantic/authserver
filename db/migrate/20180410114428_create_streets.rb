class CreateStreets < ActiveRecord::Migration[5.1]
  def change
    create_table :streets do |t|
      # General table columns
      t.string :label
      t.string :short_name
      t.string :full_name
      t.integer :street_number
      # Special table columns
      t.boolean :removed
      t.timestamps
    end
  end
end

class AddUniqeConstraintToUsers < ActiveRecord::Migration[5.1]
  def change
    add_index :users, [:email, :username, :phone], unique: true
  end
end

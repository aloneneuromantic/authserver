class CreateValueTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :value_types do |t|
      # General table columns
      t.string :label
      t.string :short_name
      t.string :full_name
      t.string :description
      # Special table columns
      t.timestamps
    end
  end
end

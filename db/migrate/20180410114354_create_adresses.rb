class CreateAdresses < ActiveRecord::Migration[5.1]
  def change
    create_table :adresses do |t|
      # General table columns
      t.string :label
      t.string :short_name
      t.string :full_name
      # Special table columns
      t.boolean :removed
      t.timestamps
    end
  end
end

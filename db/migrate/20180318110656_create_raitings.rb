class CreateRaitings < ActiveRecord::Migration[5.1]
  def change
    create_table :raitings do |t|
      # General table columns
      t.string :label
      t.string :short_name
      t.string :full_name
      t.boolean :alcohol_reference
      t.boolean :animated_blood
      t.boolean :blood
      t.boolean :blood_and_gore
      t.boolean :cartoon_violence
      t.boolean :comic_mischief
      t.boolean :crude_humor
      t.boolean :grug_reference
      t.boolean :edutainment
      t.boolean :fantasy_violence
      t.boolean :informational
      t.boolean :intense_violence
      t.boolean :cyrics
      t.boolean :mature_humor
      t.boolean :mild_lyrics
      t.boolean :mild_language
      t.boolean :mild_suggestive_themes
      t.boolean :mild_violence
      t.boolean :nudity
      t.boolean :partial_nudity
      t.boolean :real_gambling
      t.boolean :sexual_themes
      t.boolean :simulated_gambling
      t.boolean :some_adult_assistance_may_be_need
      t.boolean :strong_language
      t.boolean :strong_lyrics
      t.boolean :strong_sexual_content
      t.boolean :suggestive_themes
      t.boolean :tobacco_reference
      t.boolean :use_of_drugs
      t.boolean :use_of_tobacoo
      t.boolean :violence
      # Special table columns
      t.boolean :removed, :default => false
      t.timestamps
    end
  end
end

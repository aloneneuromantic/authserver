class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      # Global table columns
      t.string :label
      t.string :short_name
      t.string :full_name
      t.string :text
      # Special table columns
      t.boolean :removed, :default => false
      t.timestamps
    end
  end
end

class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      # General table columns
      t.string :username
      t.integer :phone, limit: 8
      t.string :email
      t.string :password
      # Special table columns
      t.boolean :removed, :default => false
      t.timestamps
    end
  end
end
6
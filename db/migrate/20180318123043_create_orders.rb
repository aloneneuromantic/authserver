class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      # Global table columns
      t.string :label
      t.string :short_name
      t.string :full_name
      t.integer :full_price
      t.integer :price_with_discount
      t.integer :total_price
      # Special table columns
      t.boolean :issued, :default => true
      t.boolean :paid_for, :default => false
      t.boolean :canceled, :default => false
      t.boolean :removed, :default => false
      t.timestamps
    end
  end
end

class CreateHomes < ActiveRecord::Migration[5.1]
  def change
    create_table :homes do |t|
      # Global table columns
      t.string :label
      t.string :short_name
      t.string :full_name
      t.string :home_number
      # Special table columns
      t.boolean :removed
      t.timestamps
    end
  end
end

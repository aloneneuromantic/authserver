class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      # General table columns
      t.string :label
      t.string :full_name
      t.string :short_name
      # Special table columns
      t.boolean :removed, :default => false
      t.timestamps
    end
  end
end

class CreateCarts < ActiveRecord::Migration[5.1]
  def change
    create_table :carts do |t|
      # General table columns
      t.string :label
      t.string :short_name
      t.string :full_name
      # Special table columns
      t.boolean :removed, :default => false
      t.timestamps
    end
  end
end

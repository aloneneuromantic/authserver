class CreateSpecialTags < ActiveRecord::Migration[5.1]
  def change
    create_table :special_tags do |t|
      # General table columns
      t.string :label
      t.string :short_name
      t.string :full_name
      t.string :description
      t.string :value
      # Special table column
      t.boolean :removed
      t.timestamps
    end
  end
end

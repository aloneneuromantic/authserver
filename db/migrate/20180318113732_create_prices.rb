class CreatePrices < ActiveRecord::Migration[5.1]
  def change
    create_table :prices do |t|
      # Global table columns
      t.string :label
      t.string :short_name
      t.string :full_name
      t.integer :value
      t.integer :share_of_developer
      t.integer :share_of_publisher
      t.boolean :discount_is_possible, :default => false
      # Special table columns
      t.boolean :removed, :default => false
      t.timestamps
    end
  end
end

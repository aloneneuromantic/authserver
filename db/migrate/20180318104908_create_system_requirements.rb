class CreateSystemRequirements < ActiveRecord::Migration[5.1]
  def change
    create_table :system_requirements do |t|
      # General table columns
      t.string :processor
      t.integer :memory
      t.string :graphics
      t.integer :hard_drive
      t.boolean :mouse
      t.boolean :keyboard
      # Special table columns
      t.boolean :removed, :default => false
      t.timestamps
    end
  end
end

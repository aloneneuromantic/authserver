require 'jwt'
# Class for universal user controller
class UniversalUserController < UniversalController
  # Method for compare two password
  def compare_passwords(user, password)
    @user = user
    @result_check = @user ? @user.check_password(password) : nil
    # Return result compare
    return @result_check if @result_check else false
  end
  # Method for change password
  def password_change(user, params)
    @user = user
    @old_password = params['old_password'] ? params['old_password'] : nil
    @new_password = params['new_password'] ? params['new_password'] : nil
    @confirm_new_password = params['confirm_new_password'] ? params['confirm_new_password'] : nil
    @result_check = compare_passwords(@user, @old_password)
    @check_new_password = @new_password == @confirm_new_password
    # If result check new password is true
    if @check_new_password
      @new_hash = @check_new_password ? @user.regenerate_password(@new_password) : nil
      @user.password = @new_hash ? @new_hash : @user.password
      @user.save
    end
    # return user or nill
    return @result_check ? @user : false
  end

  # Method to update user
  def update_user(user, params)
    @user = user
    # Check & update user data
    @user.email = params['email'] ? params['email'] : @user.email
    @user.phone = params['phone'] ? params['phone'] : @user.phone
    @user.save
    # Return user or nil
    return @user if @user else nil
  end

  # Method to get all users
  def all_users
    @users = User.all

    return @users if @users else nil
  end

  # Method to get token from params
  def get_token(params)
    @token = params['token'] ? params['token'] : nil

    return @token if @token else nil
  end

  # Method to get request bidy
  def get_request_body_users(params)
    @request_body = {
        :username => params['username'],
        :email => params['email'] ? params['email'] : nil,
        :phone => params['phone'] ? params['phone'] : nil,
        :password => params['password'] ? params['password'] : nil
    }
    # Return request body
    return @request_body if @request_body else nil
  end

  # Method to get user by username
  def get_user_by_username(username)
    @user = User.find_by_username(username)
    #Return user or nil
    return @user if @user else nil
  end

  # Method to get user by token
  def get_user_by_token(token)
    # Salt to decode token
    @hmac_secret = 'Ru$$ian_2018_$team_$ecretKey_1502'
    # Try decode token
    @decoded_token = JWT.decode token, @hmac_secret, true, { :algorithm => 'HS256' }
    # Find user by user id
    @user = User.find(@decoded_token[0]['id'])
    # Return user or nil
    return @user if @user else nil
  end

  # Check user password
  def check_password(user, password)
    @result_check = user ? user.check_password(password) : false

    return @result_check
  end

  # Get token from user
  def get_token_from_user(user, result_check)
    @token = user && result_check ? user.generate_token : nil
    return @token if @token else nil
  end
end
class UniversalController < ApplicationController
  # Method to get status by length
  def get_status_by_length(entries)
    @status = entries.length.zero? ? 404 : 200

    return @status if @status else nil
  end

  # Method for get status
  def get_status(obj, error_code)
    @status = obj.errors.messages.empty? ? 200 : error_code
    return @status if @status else 503
  end
  # Get error meta
  def get_error_meta(obj)
    @error_meta = obj.errors.messages.empty? ? nil : obj
    return @error_meta if @error_meta else nil
  end

  # Create entry
  def create_entry(model, params)
    @entry = model.create(params)
    return @entry if @entry else nil
  end
end
require 'json'

module Response
    # Define with custom error
    # Define 404 response
    def response_with_custom_error(object, meta, message)
      @errors = message
      @answer = {'data' => nil, 'error' => @errors, 'meta' => @meta }

      return @answer
    end
    # Define response with error
    def response_with_error(object, meta)
      @errors = object.errors.messages
      @data = nil
      @meta = meta
      @answer = {'data' => @data, 'error' => @errors, 'meta' => @meta }

      return @answer
    end
    def response_correct(object, meta)

      @errors = nil
      @data = object
      @meta = meta
      @answer = {'data' => @data, 'error' => @errors, 'meta' => @meta }

      return @answer
    end

    def get_error_message(status)

      @message = nil

      case status
        when 404
          @message = {'not_found' => 'Object not found in database'}
        when 400
          @message = { 'bad_request' => 'You send not correct data' }
        else
          @message = {'internal_error' => 'Internal server error'}
      end

      return @message
    end
    # Method for create answer object
    def json_response(object, status, meta)
      @answer = nil
      if object.nil?
        @message = get_error_message(status)
        @answer = response_with_custom_error(object, meta, @message)
      elsif object.errors.messages.empty?
        @answer = response_correct(object, meta)
      else
        @answer = response_with_error(object, meta)
      end

      render json: @answer, status: status
    end

end
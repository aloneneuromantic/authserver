require 'jwt'

class UsersController < UniversalUserController

  # Get all users methods
  def list
    @users = all_users
    @status = get_status_by_length(@users)
    # Return answer
    json_response(@users, @status, nil)
  end

  # Register user
  def create
    @status = nil
    @meta = nil
    @request_body = get_request_body_users(params)

    if @request_body[:password].nil?
      @status = 400
      @meta = @request_body
    else
      @user = create_entry(User, @request_body)
      @status = get_status(@user, 500)
      @meta = get_error_meta(@user)
    end
    # Return answer
    json_response(@user, @status, @meta)
  end

  # Auth user
  def login
    @user = get_user_by_username(params['username'])
    @result_check = check_password(@user, params['password'])
    @token = @result_check ? get_token_from_user(@user, @result_check) : nil
    @status = if @result_check
                @result_check && @user.errors.messages.empty? ? 200 : 400
              else
                403
              end
    @meta = @token ? { :token => @token } : params
    json_response(@result_check ? @user : nil, @status, @meta)
  end

  # Get my data
  def me
    @user = nil
    @status = nil
    @meta = nil
    @token = get_token(params)
    # Check token
    if @token.nil?
      @status = 400
    else
      @user = get_user_by_token(@token)
      # Check fined user
      if @user.nil?
        @status = 404
      else
        @meta = { :token => @token }
        @status = 200
      end
    end
    # Return answer
    json_response(@user, @status, @meta)
  end

  # Update user
  def update
    @user = nil
    @status = nil
    @meta = nil
    @token = params['token']
    # Check token
    if @token.nil?
      @status = 400
    else
      @user = get_user_by_token(@token)
      # Check user
      if @user.nil?
        @status = 404
      else
        @user = try_update_user(@user, params)
        @meta = { :token => @token }
        # Set status
        @status = get_status(@user, 403)
      end
    end
    # Return answer
    json_response(@user, @status, @meta)
  end

  # Change password
  def change_password
    @status = nil
    @meta = nil
    @token = get_token(params)
    # Check token
    if @token.nil?
      @status = 400
    else
      @user = get_user_by_token(@token)
      # Check user
      if @user.nil?
        @status = 404
      else
        @result = password_change(@user, params)
        # Save user entry
        @meta = !@result ? params : {:token => @token }
        # Set status
        @status = !@result ? 403 : 200
      end
    end
    # Return answer
    json_response(@status == 403 ? nil : @user, @status, @meta)
  end

end


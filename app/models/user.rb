require 'bcrypt'
require 'jwt'

class User < ApplicationRecord
  # Validate fields email, phone, username on unique.
  # If validated field equal nil - not validate this. field
  validates :email, :phone, :username, uniqueness: true, allow_nil: true
  # Callback before crate entry in database
  # - Create password hash & change password field on this.
  # - Set removed field on true.
  before_create do
    # Hashed password
    @password_hash = BCrypt::Password.create(self.password)
    # Set password
    self.password = @password_hash
  end
  # Regenerate password
  def regenerate_password(new_password)
    @new_hash = BCrypt::Password.create(new_password)
    return @new_hash
  end
  # Generate token
  def generate_token
    @hmac_secret = 'Ru$$ian_2018_$team_$ecretKey_1502'
    @my_info = {'id'=> self.id }
    @token = JWT.encode @my_info, @hmac_secret, 'HS256'

    return @token
  end
  # Check password
  def check_password(password)
    @hash_to_check = BCrypt::Password.new(self.password)
    @result_check = @hash_to_check == password
    return @result_check
  end
end
Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/api/v1/users/list', to:'users#list', as: 'list'
  post '/api/v1/users/register', to:'users#create', as: 'register'
  post '/api/v1/users/login', to:'users#login', as: 'login'
  post '/api/v1/users/me', to: 'users#me', as: 'me'
  post '/api/v1/users/me/update', to: 'users#update', as: 'update'
  post '/api/v1/users/me/cpwd', to: 'users#change_password', as: 'change-password'
end
